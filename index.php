<?php require_once 'db_connect.php'; ?>
<?php include 'functions.php'; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>MYsql!</title>
</head>
<style>
    .input-group {
        padding: 35px;
    }
</style>
<body>
<div class="container">
    <caption><h2 class="fw-bold">Rating for this year</h2></caption>

    <div class="col-sm-6">
        <pre><?php yearRating();
            campsRating(); ?> </pre>
    </div>

    <caption><h2 class="fw-bold">Date range </h2></caption>
    <div class="col-sm-6">
        <pre><?php averageCampRating(); ?> </pre>
    </div>

    <caption><h2 class="fw-bold">list of months</h2></caption>
    <div class="col-sm-6">
        <pre><?php averageMonth() ?> </pre>
    </div>
</div>

</body>
</html>