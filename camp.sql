-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 19 2021 г., 19:43
-- Версия сервера: 5.6.47
-- Версия PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `camp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `camps`
--

CREATE TABLE `camps` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `camps`
--

INSERT INTO `camps` (`id`, `name`) VALUES
(2, 'Camp7'),
(23, 'Laki camp'),
(24, 'Camp2'),
(25, 'Camp 3'),
(26, 'Camp 10'),
(27, 'camp 12'),
(28, 'camp 13');

-- --------------------------------------------------------

--
-- Структура таблицы `campsrating`
--

CREATE TABLE `campsrating` (
  `id` int(11) NOT NULL,
  `camp_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `campsrating`
--

INSERT INTO `campsrating` (`id`, `camp_id`, `rating`, `date`) VALUES
(2, '2', '50', '2020-12-21'),
(6, '23', '50', '2021-01-14'),
(7, '2', '83', '2021-01-09'),
(8, '26', '100', '2021-01-27'),
(23, '23', '20', '2020-12-20'),
(24, '24', '70', '2020-01-14'),
(25, '25', '100', '2019-08-14'),
(26, '27', '15', '2020-09-16'),
(27, '2', '10', '2020-11-12'),
(28, '2', '94', '2020-12-18'),
(29, '2', '46', '2020-11-19'),
(30, '2', '79', '2020-11-09'),
(31, '26', '50', '2020-11-03'),
(32, '28', '88', '2020-01-17'),
(33, '28', '66', '2020-01-23'),
(34, '28', '55', '2020-06-19'),
(35, '28', '99', '2020-06-25'),
(36, '28', '37', '2020-06-20'),
(37, '28', '64', '2020-06-18');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `camps`
--
ALTER TABLE `camps`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `campsrating`
--
ALTER TABLE `campsrating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `campsrating_id_index` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `camps`
--
ALTER TABLE `camps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `campsrating`
--
ALTER TABLE `campsrating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
