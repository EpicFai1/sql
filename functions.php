<?php

require_once 'db_connect.php';


//function that displays a list of camps
function yearRating()
{
    global $mysqli;
    $query = "SELECT camps.name, campsrating.date
             FROM campsrating
             INNER JOIN camps ON camps.id = campsrating.camp_id
             WHERE date LIKE '2020%'";
    global $result;
    $result = mysqli_query($mysqli, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}

//function that displays the average rating
function campsRating()
{
    global $mysqli;
    $query = "SELECT AVG(rating)
              FROM campsrating
              WHERE date LIKE '2020%'";
    global $result;
    $result = mysqli_query($mysqli, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}

//sorting date from and date to
function averageCampRating()
{
    global $mysqli;
    $query = "SELECT camps.name
                ,avg(campsrating.rating) as Rating
                FROM campsrating
                INNER JOIN camps ON camps.id = campsrating.camp_id
                WHERE date BETWEEN '2020-01-01' AND '2021-01-31'
                GROUP BY camps.id,camps.name";
    $result = mysqli_query($mysqli, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}

//
function averageMonth()
{
    global $mysqli;
    $query = "SELECT camps.name, MONTH(date) as month, avg (rating) as Rating
                FROM campsrating
                INNER JOIN camps ON camps.id = campsrating.camp_id
                WHERE date LIKE '2020%'
                GROUP BY MONTH(date), camps.name";
    $result = mysqli_query($mysqli, $query);
    while ($row = mysqli_fetch_assoc($result))
        print_r($row);
}